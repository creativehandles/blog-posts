<?php

namespace Creativehandles\BlogPosts\Repositories;

use Creativehandles\BlogPosts\Http\Resources\PostResourceCollection;
use App\Repositories\BaseEloquentRepository;
use Creativehandles\BlogPosts\Http\Resources\PostResource;
use Creativehandles\BlogPosts\Models\Post;

class PostsRepository extends BaseEloquentRepository{


    public function getModel()
    {
        return new Post();
    }

    public function getResource()
    {
        return PostResource::class;
    }

    public function getResourceCollection()
    {
        return PostResourceCollection::class;
    }

    public function getDataforDataTables(array $searchColumns = null, array $orderColumns = null, string $defOrderColumnKey = null, array $with = null, array $where= null){
        return parent::getDataforDataTables($searchColumns,$orderColumns,$defOrderColumnKey,$with,$where);
    }


    public function getDataIndexforDataTables($collection)
    {
        return $collection;
    }
}
