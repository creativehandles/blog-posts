<?php

namespace Creativehandles\BlogPosts\Models;

use App\Traits\ExtendedHasTranslationTrait;
use App\Traits\HasCategories;
use App\Traits\HasImages;
use App\Traits\HasTags;
use App\Traits\HasAuthor;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;
use Illuminate\Support\Str;

class Post extends Model
{
    use HasAuthor;
    use Mediable;
    use ExtendedHasTranslationTrait;
    use HasTags,HasCategories,HasImages;

    protected $appends = ['available_langs'];

    protected $guarded = [];

    protected $table = "ch_posts";

    protected $hidden = [
        "deleted_by",
        "deleted_at"
    ];

    public $translatable = [
        'title',
        'short_description',
        'main_description',
        'additional_description',
        'slug',
        'status',
        'reading_time',
        'seo_title',
        'seo_description',
        'seo_url',
        'seo_image_url',
        'seo_keywords'
    ];

    public function setslugAttribute($value)
    {
        $value = $value ?: $this->attributes['title'];
        $this->attributes['slug'] = Str::slug($value);
    }
    
}
