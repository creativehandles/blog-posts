<?php

namespace Creativehandles\BlogPosts;

class BlogPosts
{
    // Build your next great package.

    public static function getIndexRoute()
    {
        return config('blog-posts.IndexRoute', "admin.posts.index");
    }

    public static function getCreateRoute()
    {
        return config('blog-posts.CreateRoute', "admin.posts.create");
    }

    public static function getStoreRoute()
    {
        return config('blog-posts.StoreRoute', "admin.posts.store");
    }

    public static function getEditRoute()
    {
        return config('blog-posts.EditRoute', "admin.posts.edit");
    }

    public static function getUpdateRoute()
    {
        return config('blog-posts.UpdateRoute', "admin.posts.update");
    }

    public static function getDeleteRoute()
    {
        return config('blog-posts.DeleteRoute', "admin.posts.delete");
    }

    public static function getDestroyRoute()
    {
        return config('blog-posts.DestroyRoute', "admin.posts.destroy");
    }

    public static function getGridRoute()
    {
        return config('blog-posts.GridRoute', "admin.posts.grid");
    }

    /** add the name of the resource as in routes
     * ex : Route::resource('admin/posts', 'PostsController')->name('posts');
     */
    public static function getResourceName()
    {
        return config('blog-posts.resourceName', "posts");
    }

    public static function getStatuses()
    {
        return [
            "Draft" => __('posts.Draft'),
            "Pending" => __('posts.Pending'),
            "Published" => __('posts.Published')
        ];
    }
}
