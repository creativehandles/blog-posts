<?php

namespace Creativehandles\BlogPosts\Services;

use App\Repositories\CoreRepositories\UserRepository;

class PostPluginService{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAuthors(){
        return $this->userRepository->getAuthors();
    }
}
