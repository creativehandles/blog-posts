<?php

namespace Creativehandles\BlogPosts\Traits;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Plank\Mediable\Facades\MediaUploader;
use Plank\Mediable\Media;
use Plank\Mediable\SourceAdapters\SourceAdapterInterface;

trait MediaManagerTrait{

    public $allowedFileSize = 1024 * 1024 * 10; //10MB max in Bytes



    //get filesystem
    public function getFileSystem(){
        return config("mediable.default_disk");
    }


    public function validateUploadingFile(Request $request, $identifier = 'file'){

       $request->validate([
            $identifier => 'max:'.$this->allowedFileSize,
        ]);

    }

    //process request
    public function processOne(Request $request, $fieldName,$directory="media"){

        return MediaUploader::fromSource($request->file($fieldName))
        ->toDestination($this->getFileSystem(),$directory)
        ->setMaximumSize($this->allowedFileSize)
        ->beforeSave(function(Media $model, SourceAdapterInterface $source){
            $model->setAttribute('locale', app()->getLocale());
        })
        ->upload();
    }


    //upload multiple files
    public function processMany(Request $request, $fieldName,$directory="uploads"){
        $imageList = $request->file($fieldName, []);

        $uploadedIds = [];
        $uploadedModels = [];

        foreach($imageList as $image){

           $media =  MediaUploader::fromSource($image)
                    ->toDestination($this->getFileSystem(),$directory)
                    ->setMaximumSize($this->allowedFileSize)
                    ->beforeSave(function(Media $model, SourceAdapterInterface $source){
                        $model->setAttribute('locale', app()->getLocale());
                    })
                    ->upload();

        $uploadedIds[]  = $media->getKey();
        $uploadedModels[]  = $media;

        }


        return [$uploadedIds,$uploadedModels];
    }




}
