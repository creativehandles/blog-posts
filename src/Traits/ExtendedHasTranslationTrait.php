<?php

namespace Creativehandles\BlogPosts\Traits;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Spatie\Translatable\HasTranslations;

trait ExtendedHasTranslationTrait{

    use HasTranslations;



    public function toArray()
    {
        $attributes = parent::toArray();
        foreach ($this->getTranslatableAttributes() as $field) {
            $attributes[$field] = $this->getTranslation($field, app()->getLocale());
        }
        return $attributes;
    }

    public function getAvailableLangsAttribute()
    {
        return array_keys($this->getTranslations($this->translatable[0]));
    }

}
