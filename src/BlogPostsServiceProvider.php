<?php

namespace Creativehandles\BlogPosts;

use Carbon\Carbon;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class BlogPostsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'blog-posts');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'blog-posts');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('blog-posts.php'),
                // __DIR__ . '/../config/mediable.php' => config_path('mediable.php'),
            ], 'config');

            // Publishing  controller.
            $this->publishes([
                __DIR__ . '/../app/Http/Controllers/' => app_path('/Http/Controllers/'),
            ], 'pluginController');


            // Publishing the views.
            $this->publishes([
                __DIR__ . '/../resources/views' => resource_path('views/Admin/Posts/'),
            ], 'views');

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/blog-posts'),
            ], 'assets');*/

            // Publishing the translation files.
            $this->publishes([
                __DIR__ . '/../resources/lang' => resource_path('lang/'),
            ], 'lang');

            // Registering package commands.
            // $this->commands([]);

            //publishing routes and breadcrumbs
            $this->publishes([
                __DIR__ . '/../routes/web.php' => base_path('routes/packages/package-routes/blog-posts-web-routes.php'),
                __DIR__ . '/../routes/api.php' => base_path('routes/packages/api-routes/blog-posts-api-routes.php'),
                __DIR__ . '/../routes/breadcrumbs' => base_path('routes/packages/breadcrumbs/'),
            ], 'routes');

        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'blog-posts');

        // Register the main class to use with the facade
        $this->app->singleton('blog-posts', function () {
            return new BlogPosts;
        });

        // $this->registerRoutes();
    }

    protected function registerRoutes()
    {

        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        });

        Route::group($this->apiRouteConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');
        });
    }

    protected function routeConfiguration()
    {
        return [
            'prefix' => config('blog-posts.web.route_prefix'),
            'middleware' => config('blog-posts.web.middleware'),
        ];
    }


    protected function apiRouteConfiguration()
    {
        return [
            'prefix' => config('blog-posts.api.route_prefix'),
            'middleware' => config('blog-posts.api.middleware'),
        ];
    }
}
