<?php

namespace Creativehandles\BlogPosts\Http\Controllers\PluginControllers;

use Creativehandles\BlogPosts\BlogPosts;
use App\Http\Controllers\BaseControllers\BasePluginController;
use App\Traits\ManageCategoriesTrait;
use App\Traits\ManageTagsTrait;
use Creativehandles\BlogPosts\Repositories\PostsRepository;
use Creativehandles\BlogPosts\Services\PostPluginService;
use App\Traits\MediaManagerTrait;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PostPluginController extends BasePluginController
{
    use MediaManagerTrait, ManageCategoriesTrait, ManageTagsTrait;


    public function getService()
    {
        return app(PostPluginService::class);
    }

    public function getRepository()
    {
        return app(PostsRepository::class);
    }

    public function getViewsFolder($override = null)
    {
        return "Admin.Posts.";
    }

    public function getIndexRoute()
    {
        return BlogPosts::getIndexRoute();
    }

    public function getCreateRoute()
    {
        return "posts.create";
    }

    public function getEditRoute()
    {
        return "posts.edit";
    }

    public function getDeleteRoute()
    {
        return "posts.delete";
    }

    /** add the name of the resource as in routes
     * ex : Route::resource('admin/posts', 'PostsController')->name('posts');
     */
    public function getResourceName()
    {
        return "posts";
    }


    public function validateRequest(Request $request)
    {
        $request->validate([
            // 'slug' => 'required|unique_translation:ch_posts,slug,' . $request->id,
            'title' => "required",
            'main_description' => "required",
            'status' => "required",
            'seo_url' => "required",
        ]);
    }

    /**
     * Show the form for creating a new Items.
     *
     * @return Response
     */
    public function create()
    {
        $statuses = BlogPosts::getStatuses();
        $authors = $this->service->getAuthors();
        return view($this->viewsFolder . 'add', ['statuses' => $statuses])->with('authors', $authors);
    }


    /**
     * Show the form for editing the specified Items.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        try {
            $items = $this->repository->byId($id);

            $statuses = BlogPosts::getStatuses();
            $authors = $this->service->getAuthors();
        } catch (ModelNotFoundException $e) {
            \Session::flash('error', __('Item not found'));
            return redirect(route($this->indexRoute));
        } catch (Exception $e) {
            \Session::flash('error', __('Error Occured'));
            return redirect(route($this->indexRoute));
        }


        return view($this->viewsFolder . 'add')->with('model', $items)
            ->with('authors', $authors)
            ->with('statuses', $statuses);
    }

    /**
     * Update the specified Items in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validateRequest($request);

        try {
            $formatData = $request->except(['multi_attachments', 'multiImageList', 'categories', 'tags']);

            if ($id) {
                $formatData['id'] = $id;
            }

            $items = $this->repository->updateOrCreate($formatData, 'id');

            $categories = $request->get('categories', []);
            $tags = $request->get('tags', []);

            //sync categories
            $this->updateCategories($items, $categories);
            //sync tags
            $this->updateTags($items, $tags);

            $imageList = $request->get('multiImageList', []); //here we are getting an array of images

            if ($imageList) {
                //link images
                if (count($imageList) > 0) {
                    $this->processImages($imageList, $items);
                } else {
                    $items->images()->where('model', $this->model)->delete();
                }
            } else {
                $items->images()->where('model', $this->model)->delete();
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => [__('Error occurred')], 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => __('Item updated successfully.'), 'data' => $items], 200);
        } else {
            \Session::flash("success", __('Item updated successfully.'));
            return redirect(route($this->indexRoute));
        }
    }

    public function grid(Request $request)
    {
        try {

            $searchColumns = ['ch_posts.id', 'ch_posts.title'];
            $orderColumns = ['1' => 'ch_posts.id'];
            $with = [];
            $data = $this->repository->getDataforDataTables($searchColumns, $orderColumns, '1', $with);
            $translateBtns = "";
            $forgetBtns = "";

            $data['data'] = $data['data']->map(function ($item) use ($translateBtns, $forgetBtns) {
                $return = [
                    $item->id,
                    $item->title,
                    $item->slug,
                    $item->date,
                    __('posts.' . $item->status),
                    $item->author ? $item->author->name : __('posts.N/A'),
                    $this->translateButton($item, BlogPosts::getEditRoute(), ['post' => $item->id]),
                    $this->editButton($item, BlogPosts::getEditRoute(), ['post' => $item->id]),
                    $this->deleteButton($item, BlogPosts::getDestroyRoute()),
                ];

                return $return;
            });

            return json_encode($data);
        } catch (Exception $e) {
            return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage() . $e->getTraceAsString()], 500);
        }
    }
}
