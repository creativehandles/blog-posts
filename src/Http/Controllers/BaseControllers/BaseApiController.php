<?php

namespace Creativehandles\BlogPosts\Http\Controllers\BaseControllers;

use App\Http\Controllers\Controller;
use App\Services\Response\ExternalApiResponse;
use Exception;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Http\Request;

abstract class BaseApiController extends Controller
{

    protected $processingService;

    protected $response;

    protected $repository;

    protected $resource;

    protected $service;

    /**
     * Create a new controller instance.
     *
     * @param $response
     * @param  null  $repository
     */
    public function __construct(ExternalApiResponse $response)
    {
        $this->response = $response;


        $this->repository = $this->getRepository();
        $this->resource = $this->repository->getResource();
    }


    abstract public function getService();

    abstract public function getRepository();


    public function index(Request $request)
    {
        try {
            $results = $this->repository->all($request->input('with', []));
        } catch (RelationNotFoundException $e) {
            return $this->response->failed($e->getMessage(), 401, 'Invalid relationship defined', $e->getMessage());
        } catch (\Exception $e) {
            return $this->response->failed($e->getMessage());
        }

        return $this->response->success($this->resource::collection($results));
    }

    public function show(Request $request, $id)
    {
        try {
            $results = $this->repository->byId($id, $request->input('with', []));
        } catch (\Exception $exception) {
            return $this->response->failed('No records found', 404, $exception->getMessage(), $exception->getCode());
        }

        $resource = new $this->resource($results);

        return $this->response->success($resource);
    }

    public function destroy($id)
    {
        $deleted = $this->repository->deleteById($id);

        if (!$deleted) return $this->response->failed(__('general.Error Occured'), 400);

        return $this->response->success('Deleted');
    }

    /**
     * @param  Request  $request
     * @return mixed
     */
    public function store(Request $request)
    {
        if ($request instanceof Request) {
            $data = $request->all();
        } elseif (is_array($request)) {
            $data = $request;
        } else {
            throw new Exception('request parameter should be either an object of Illuminate\Http\Request class or an array.');
        }

        return $this->repository->create($data);
    }

    /**
     * @param  Request  $request
     * @param $id
     */
    public function update($request, int $id)
    {
        if ($request instanceof Request) {
            $data = $request->all();
        } elseif (is_array($request)) {
            $data = $request;
        } else {
            throw new Exception('request parameter should be either an object of Illuminate\Http\Request class or an array.');
        }

        return $this->repository->updateById($data, $id);
    }


    public function processUpdateOrCreate(array $values, $columns = null)
    {
        return $this->repository->updateOrCreate($values, $columns);
    }
}
