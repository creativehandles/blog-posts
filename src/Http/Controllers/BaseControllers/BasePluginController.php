<?php

namespace Creativehandles\BlogPosts\Http\Controllers\BaseControllers;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use ReflectionClass;

abstract class BasePluginController extends Controller
{


    protected $viewsFolder;

    protected $repository;

    protected $resource;

    protected $service;

    public $model;

    protected $resourceName;
    protected $indexRoute;
    protected $createRoute;
    protected $editRoute;
    protected $deleteRoute;
    /**
     * Create a new controller instance.
     *
     * @param $response
     * @param  null  $repository
     */
    public function __construct()
    {

        //if request has translating_lang, then set the app locale to requested type
        $locale = request()->get('translating_lang', app()->getLocale());
        app()->setLocale($locale);
        //remove this header after setting locale. if someone wants this is method level, feel free to change
        request()->request->remove('translating_lang');

        //model related service
        $this->service = $this->getService();

        //related repository
        $this->repository = $this->getRepository();

        //views repository
        $this->viewsFolder = $this->getViewsFolder();

        //routes should be specified
        $this->resourceName = $this->getResourceName();

        $this->indexRoute = $this->getIndexRoute();
        $this->createRoute = $this->getCreateRoute();
        $this->editRoute = $this->getEditRoute();
        $this->deleteRoute = $this->getDeleteRoute();

        $this->model = $this->getModel();
    }

    public function getModel()
    {
        $reflector = new ReflectionClass($this->getRepository()->getModel());
        return $reflector->getShortName();
    }


    abstract public function getService();

    abstract public function getRepository();

    abstract public function getViewsFolder();

    abstract public function getIndexRoute();
    abstract public function getCreateRoute();
    abstract public function getEditRoute();
    abstract public function getDeleteRoute();

    abstract public function validateRequest(Request $request);

    /** add the name of the resource as in routes
     * ex : names attribute
     * Route::resource('/posts', 'Creativehandles\BlogPosts\Http\Controllers\ApiControllers\PostApiController',['names' => 'posts']);;
     *
     */
    abstract public function getResourceName();


    /**
     * Undocumented function
     *
     * @param Model|null $item
     * @param [string] $editRoute
     * @param [array] $routeParams should be in format ['post' => $item->id, 'translating_lang' => $key ]
     * @return void
     */
    public  function translateButton(Model $item = null, $translateRoute, array $routeParams)
    {

        $options = "";

        foreach (config('laravellocalization.supportedLocales') as $key => $name) {
            $options .= '<a class="dropdown-item" href="' . route($translateRoute, array_merge($routeParams, ['translating_lang' => $key])) . '">' . strtoupper($key) . '</a>';
        }

        return '<div class="btn-group mr-1 mb-1">
            <button type="button" class="btn btn-warning btn-min-width dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . __("general.Translate") . '</button>
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 42px, 0px);">
            ' . $options . '
            </div>
            </div>';
    }

    /**
     * Undocumented function
     *
     * @param Model|null $item
     * @param [string] $editRoute
     * @param [array] $routeParams should be in format ['post' => $item->id, 'translating_lang' => $key ]
     * @return void
     */
    public  function editButton(Model $item = null, $editRoute, array $routeParams)
    {

        $options = "";

        foreach ($item->available_langs as $key => $name) {
            $options .= '<a class="dropdown-item" href="' . route($editRoute, array_merge($routeParams, ['translating_lang' => $key])) . '">' . strtoupper($name) . '</a>';
        }

        return '<div class="btn-group mr-1 mb-1">
            <button type="button" class="btn btn-info btn-min-width dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . __("general.Update") . '</button>
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 42px, 0px);">
            ' . $options . '
            </div>
            </div>';
    }

    public function deleteButton(Model $item = null)
    {

        $options = "";

        foreach ($item->available_langs as $key => $name) {
            $options .= '<a class="dropdown-item deleteTranslation" data-id="' . $item->id . '" data-lang="' . $name . '" >' . strtoupper($name) . '</a>';
        }


        return '<div class="btn-group mr-1 mb-1">
            <button type="button" class="btn btn-danger btn-min-width dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . __("general.Delete") . '</button>
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 42px, 0px);">
            ' . $options . '
                <div class="dropdown-divider"></div>
                <a class="dropdown-item deleteTranslation" data-id="' . $item->id . '"  >All</a>
            </div>
        </div>';
    }

    /**
     * Display a listing of the Items.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {


        $items = $this->repository->all();

        return view($this->viewsFolder . 'index')
            ->with('items', $items);
    }

    /**
     * Show the form for creating a new Items.
     *
     * @return Response
     */
    public function create()
    {
        return view($this->viewsFolder . 'add');
    }

    /**
     * Store a newly created Items in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->update(null, $request);
    }

    /**
     * Display the specified Items.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {

        //if request has translating_lang, then set the app locale to requested type
        $locale = request()->get('translating_lang', app()->getLocale());
        app()->setlocale($locale);


        $items = $this->repository->find($id);

        if (empty($items)) {
            \Session::flash("error", __('Item not found'));

            return redirect(route($this->indexRoute));
        }

        return view($this->viewsFolder . 'show')->with('items', $items);
    }

    /**
     * Show the form for editing the specified Items.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {

        $items = $this->repository->find($id);

        if (empty($items)) {
            \Session::flash("error", __('Item not found'));

            return redirect(route($this->indexRoute));
        }

        return view($this->viewsFolder . 'add')->with('model', $items);
    }

    /**
     * Update the specified Items in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {

        try {
            $formatData = $request->all();

            if ($id) {
                $formatData['id'] = $id;
            }

            $items = $this->repository->updateOrCreate($formatData, 'id');


            if ($request->ajax()) {
                return response()->json(['msg' => __('Item updated successfully.'), 'data' => $items], 200);
            } else {
                \Session::flash("success", __('Item updated successfully.'));
                return redirect(route($this->indexRoute));
            }
        } catch (Exception $e) {
            if ($request->ajax()) {
                return response()->json(['msg' => ['Error occurred'], 'developer_msg' => $e->getMessage()], 500);
            } else {
                return redirect()->back()->withInput()->with('error', $e->getMessage())->withInput();
            }
        }

        if ($request->ajax()) {
            return response()->json(['msg' => __('Item updated successfully.'), 'data' => $items], 200);
        } else {
            \Session::flash("success", __('Item updated successfully.'));
            return redirect(route($this->indexRoute));
        }
    }

    /**
     * Remove the specified Items from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $items = $this->repository->find($id);

        if (empty($items)) {
            if (request()->ajax()) {
                return response()->json(['msg' => __('Item not found.'), 'data' => $items], 404);
            }

            \Session::flash("error", __('Item not found'));
            return redirect(route($this->indexRoute));
        }

        if ($lang = request()->get('delete_lang')) {
            $items->forgetAllTranslations($lang)->save();
        } else {
            $this->repository->deleteById($id);
        }


        if (request()->ajax()) {
            return response()->json(['msg' => __('Item Deleted successfully.'), 'data' => $items], 200);
        } else {
            \Session::flash("success", __('Item Deleted successfully.'));
            return redirect(route($this->indexRoute));
        }
    }

    public function grid(Request $request)
    {
        return [];
    }
}
