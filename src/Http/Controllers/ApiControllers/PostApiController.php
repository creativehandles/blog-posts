<?php

namespace Creativehandles\BlogPosts\Http\Controllers\ApiControllers;

use App\Http\Controllers\BaseControllers\BaseApiController;
use Creativehandles\BlogPosts\Repositories\PostsRepository;
use Creativehandles\BlogPosts\Services\PostApiService;

class PostApiController extends BaseApiController
{

    public function getSearchableColumns()
    {
        return ["title",
        "short_description",
        "main_description",
        "additional_description"];//['products_title', 'products_short_description']
    }

    public function getOrderableColumns()
    {
        return ["ch_posts.id"=>"id","ch_posts.title"=>"title"];//['products.id' => 'id', "products.products_title" => "products_title"];
    }

    public function getDefaultRelations()
    {
        return ["categories","tags"];
    }
    
    public function getService()
    {
        return app(PostApiService::class);
    }

    public function getRepository()
    {
        return app(PostsRepository::class);
    }
}
