<?php

namespace Creativehandles\BlogPosts\Http\Resources;

use App\Http\Resources\AuthorResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CoreJsonResource;
use App\Http\Resources\RelatedImageResource;
use App\Http\Resources\TagResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends CoreJsonResource
{
/**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "title" => $this->title,
            "short_description" => $this->short_description,
            "main_description" => $this->main_description,
            "additional_description" => $this->additional_description,
            // "image_url" => $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first() ? $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first()->getUrl() : "",
            // "image_full_url" => $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first() ? $this->getMediaMatchAll(['image_url-'.app()->getLocale()])->first()->getUrl() : "",
            // $this->image_url, // something to work around
            "image_url" => url($this->image_url),
            "image_path" => $this->image_url,
            "slug" => $this->slug,
            "date" => $this->date,
            "status" => $this->status,
            "author_id" => $this->author_id,
            "reading_time" => $this->reading_time,
            "seo_title" => $this->seo_title,
            "seo_description" => $this->seo_description,
            "seo_url" => $this->seo_url,
            "seo_keywords" => $this->seo_keywords,
            "available_langs" => $this->available_langs,
            "images" => RelatedImageResource::collection($this->whenLoaded("images")),
            "author" => new AuthorResource($this->whenLoaded("author")),
            "categories" => CategoryResource::collection($this->categories),
            "tags" => TagResource::collection($this->tags),
        ];
    }

}
