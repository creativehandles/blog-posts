<?php

namespace Creativehandles\BlogPosts\Http\Resources;

use App\Http\Resources\BaseResourceCollection;
use Creativehandles\BlogPosts\Http\Resources\PostResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PostResourceCollection extends BaseResourceCollection
{

    public function getResourceClass(): string
     {
       return  PostResource::class;
     }
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
