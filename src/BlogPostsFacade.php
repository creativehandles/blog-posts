<?php

namespace Creativehandles\BlogPosts;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\BlogPosts\Skeleton\SkeletonClass
 */
class BlogPostsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'blog-posts';
    }
}
