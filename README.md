# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/creativehandles/blog-posts.svg?style=flat-square)](https://packagist.org/packages/creativehandles/blog-posts)
[![Total Downloads](https://img.shields.io/packagist/dt/creativehandles/blog-posts.svg?style=flat-square)](https://packagist.org/packages/creativehandles/blog-posts)
![GitHub Actions](https://github.com/creativehandles/blog-posts/actions/workflows/main.yml/badge.svg)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require creativehandles/blog-posts
```

## Usage
Publish required files (migrations,routes,views etc) to CORE CMS

```php
php artisan vendor:publish --provider="Creativehandles\BlogPosts\BlogPostsServiceProvider"
```

Add following environment variable. This is a global variable for all packages.Recommends to have it in CORE-CMS. DO NOT CHANGE

```php
MEDIABLE_DISK=mediable
```

Migrate database tables
```php
php artisan migrate
```

Config file
```php
    "api" => [
        'route_prefix' => 'api/v1/',
        'middleware' => ['api']
    ],
    "web" => [
        'route_prefix' => 'plugins/',
        'middleware' => ['web']
    ],


    "IndexRoute" => "admin.posts.index",
    "CreateRoute" => "admin.posts.create",
    "EditRoute" => "admin.posts.edit",
    "DeleteRoute" => "admin.posts.delete",

    "resourceName" => "posts",

    "StoreRoute" => "admin.posts.store",
    "UpdateRoute" => "admin.posts.update",
    "DestroyRoute" => "admin.posts.destroy",
    "GridRoute" => "admin.posts.grid",
```
### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email deemantha@creativehandles.com instead of using the issue tracker.

## Credits

-   [Deemantha](https://github.com/creativehandles)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
