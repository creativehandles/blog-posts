<?php

/*
 * You can place your custom package configuration in here.
 */
return [

    "api" => [
        'route_prefix' => 'api/v1/',
        'middleware' => ['api']
    ],
    "web" => [
        'route_prefix' => 'plugins/',
        'middleware' => ['web']
    ],


    "IndexRoute" => "admin.posts.index",
    "CreateRoute" => "admin.posts.create",
    "EditRoute" => "admin.posts.edit",
    "DeleteRoute" => "admin.posts.delete",

    "resourceName" => "posts",

    "StoreRoute" => "admin.posts.store",
    "UpdateRoute" => "admin.posts.update",
    "DestroyRoute" => "admin.posts.destroy",
    "GridRoute" => "admin.posts.grid",

    'media' => [
        // The allowed mimetypes to be uploaded
        // 'allowed_mimetypes' => '*', //All types can be uploaded
        'allowed_mimetypes_images' => [
          'image/jpeg',
          'image/jpg',
          'image/png',
          'image/gif',
          'image/bmp',
        ],

        'allowed_mimetypes_videos' =>[
            'video/mp4',
        ],
        //Path for media. Relative to the filesystem.
        'path'                => '/',
        'show_folders'        => true,
        'allow_upload'        => true,
        'allow_move'          => true,
        'allow_delete'        => true,
        'allow_create_folder' => true,
        'allow_rename'        => true,
/**
       'thumbnails'          => [
           [
                'type'  => 'fit',
                'name'  => 'fit-500',
                'width' => 500,
                'height'=> 500
           ],
       ]*/
    ],
];
