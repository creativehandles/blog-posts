<?php

return [
    "Posts"=>"Posts",
    "Update" => "Update Post",
    "Post content" => "Post Content",
    "Create new" => "Create New",
    "title" => "Title",
    "short_description" => "Short Description",
    "main_description" => "Main Description",
    "additional_description" => "Additional Description",
    "slug" => "Slug",
    "date" => "Date",
    "status" => "Status",
    "Draft" => "Draft",
    "Pending" => "Pending",
    "Published" => "Published",
    "author_id" => "Author",
    "Please select" => "Please select",
    "image_url"=>"Main Image",
    "published_date"=>"Published Date",
    "author"=>"Author",
    "N/A"=>"N/A",
    ""=>"-"
];
