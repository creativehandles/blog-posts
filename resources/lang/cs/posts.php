<?php

return [
    "Posts"=>"Příspěvky",
    "Update" => "Aktualizovat příspěvek",
    "Post content" => "Obsah příspěvku",
    "Create new" => "Přidat nový",
    "title" => "Titulek",
    "short_description" => "Krátký popis",
    "main_description" => "Hlavní popis",
    "additional_description" => "Další popis",
    "slug" => "Identifikátor",
    "date" => "Datum",
    "status" => "Stav",
    "Draft" => "Koncept",
    "Pending" => "Skryto",
    "Published" => "Publikováno",
    "author_id" => "Autor",
    "Please select" => "Vyberte prosím",
    "image_url"=>"Hlavní obrázek",
    "published_date"=>"Datum zveřejnění",
    "author"=>"Autor",
    "N/A"=>"Bez autora",
    ""=>"-"
];
