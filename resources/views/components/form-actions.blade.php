{{-- <input type="hidden" name="translating_lang" value="{{request()->get('translating_lang', app()->getLocale())}}">

<div class="form-actions left">
    <button type="reset" class="btn btn-warning mr-1">
        <i class="ft-x"></i> {{__('trainings.general.reset')}}
    </button>

    <a href="{{ URL::previous() }}">
        <button type="button" href="" class="btn btn-warning mr-1">
            <i class="ft-arrow-left"></i> {{__('trainings.general.goBack')}}
        </button></a>
    <button type="button" class="btn btn-primary" id="saveForm">
        <i class="fa fa-check-square-o"></i> {{__('trainings.general.update')}}
    </button>
    {{-- <button type="submit" class="btn btn-primary" id="saveAndGoBack">
        <i class="fa fa-check-square-o"></i> {{__('trainings.general.updateAndBack')}}
    </button> --}}
</div>


@push('component-scripts')
<script>
var saveFormBtn = $('#saveForm');
var saveBtnOriginal = saveFormBtn.html();

saveFormBtn.on('click', function (e) {
    var form=$(this).parents('form:first');
    e.preventDefault();
    saveFormBtn.html('<i class="fa fa-spinner"></i> Processing');

    $.ajax({
        data: new FormData(form[0]),
        dataType: 'json',
        beforeSend: function(request) {
            request.setRequestHeader("Accept", "application/json");
            // request.setRequestHeader("Content-Type", "application/json");
        },
        processData: false,  // Important!
        contentType: false,
        method: form.attr('method'),
        url: form.attr('action'),
    }).done(function (response) {

        saveFormBtn.html(saveBtnOriginal);

        form.find('#id').val(response.data.id);
        toastr.success(response.msg, window.successMsg, {
            timeOut: 5000,
            fadeOut: 1000,
            progressBar: true,
        });

    }).fail(function (error) {
        saveFormBtn.html(saveBtnOriginal);
var messages = error.responseJSON.errors ? error.responseJSON.errors : error.responseJSON.msg;

for (message in messages) {
    toastr.error(messages[message], window.errorMsg);
}

    });
});
</script>
@endpush --}}
