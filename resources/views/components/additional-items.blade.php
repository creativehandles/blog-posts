<h4 class="form-section"><i class="fa fa-folder"></i>{{ __('general.Additional Content') }}</h4>

<div class="form-group">
    <label for="reading_time" class="col-sm-12 control-label">{{__('general.reading_time')}}</label>
    <div class="col-sm-12">
        <input required type="text" name="reading_time" id="reading_time" class="form-control" value="{{isset($model) ? $model->reading_time : (old('reading_time'))?? '' }}">
    </div>
</div>

