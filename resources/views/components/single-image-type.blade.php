<div class="form-group">
    <label for="image_url" class="col-sm-3 control-label">{{__('posts.image_url')}}</label>
    <div class="col-sm-12">
        <input type="file" accept="{{ implode(",",config('blog-posts.media.allowed_mimetypes_images'))}}" name="{{$filedName}}" id="{{$filedName}}" class="form-control" value="{{isset($model) ? $model->$filedName : (old($filedName))?? '' }}">
        <img id="{{$filedName}}_preview" width="150" height="150"  style="display: block;max-width:250px;max-height:150px;width: auto;height: auto;" class="mt-1 img-thumbnail " src="{{ isset($model) && $model->getMediaMatchAll([$filedName."-".app()->getLocale()])->first() ? $model->getMediaMatchAll([$filedName."-".app()->getLocale()])->first()->getUrl() : ""}}" width="300px">
    </div>
</div>

@push('component-scripts')
<script>

const fieldName = "{{$filedName}}";

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // console.log(e.target.result);
            // $("#page_image").append('<img class="height-150 img-thumbnail" src="'+e.target.result+'">')
            $('#'+fieldName+'_preview').attr('src', e.target.result);
            $('#'+fieldName+'_preview').show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#"+fieldName).change(function() {
    readURL(this);
});
</script>
@endpush
