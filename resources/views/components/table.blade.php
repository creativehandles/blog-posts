<div class="row">
    <div class="col-md-12 col-12 mb-2">
        <div class="mb-2 pull-left">
            @component("Admin.components.language-picker")
            @endcomponent
        </div>
        <div class="mb-2 pull-right">
            <a href="{{route('admin.posts.create')}}" class="btn btn-secondary btn-block-sm"><i class="ft-file-plus"></i> {{__('posts.Create new')}}</a>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped full-width" id="thegrid">
        <thead>
        <tr>
            <th>{{__(config('blog-posts.resourceName').'.id')}}</th>
            <th>{{__(config('blog-posts.resourceName').'.title')}}</th>
            <th>{{__(config('blog-posts.resourceName').'.slug')}}</th>
            <th>{{__(config('blog-posts.resourceName').'.published_date')}}</th>
            <th>{{__(config('blog-posts.resourceName').'.status')}}</th>
            <th>{{__(config('blog-posts.resourceName').'.author')}}</th>
            <th class="action-col">{{__('general.Translate')}}</th>
            <th class="action-col" >{{__('general.Update')}}</th>
            <th class="action-col" >{{__('general.Delete')}}</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
