@extends('Admin.layout')
@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    @include('Admin.partials.form-alert')
    @include('Admin.partials.breadcumbs', ['header' => __('posts.Posts')])
    <div class="content-body">
        @if (isset($model))
            <form action="{{ route(BlogPosts::getUpdateRoute(), ['post' => $model->id]) }}" method="POST"
                class="form-horizontal" enctype="multipart/form-data">
            @else
                <form action="{{ route(BlogPosts::getStoreRoute()) }}" method="POST" class="form-horizontal"
                    enctype="multipart/form-data">
        @endif

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        @if (isset($model))
                            <h4 class="card-title" id="basic-layout-form-center">{{ __('posts.Update') }}</h4>
                        @else
                            <h4 class="card-title" id="basic-layout-form-center">{{ __('posts.Create new') }}</h4>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 border-right-blue-grey border-right-lighten-5">


                                {{ csrf_field() }}

                                @if (isset($model))
                                    <input type="hidden" name="_method" value="PATCH">

                                    <input type="hidden" name="id" id="id" class="form-control"
                                        readonly="readonly" value="{{ $model->id }}">
                                @else
                                    <input type="hidden" name="id" id="id" class="form-control"
                                        readonly="readonly" value="">
                                @endif
                                <h4 class="form-section"><i class="fa fa-file-o"></i>{{ __('posts.Post content') }}</h4>
                                <div class="form-group">
                                    <label for="title"
                                        class="col-sm-12 control-label required">{{ __('posts.title') }}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="title" id="title" class="form-control"
                                            value="{{ isset($model) ? $model->title : old('title') ?? '' }}">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="short_description"
                                        class="col-sm-12 control-label">{{ __('posts.short_description') }}</label>
                                    <div class="col-sm-12">
                                        <textarea rows="5" required name="short_description" id="short_description" class="form-control">{{ isset($model) ? $model->short_description : old('short_description') ?? '' }}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="main_description"
                                        class="col-sm-12 control-label required">{{ __('posts.main_description') }}
                                    </label>
                                    <div class="col-sm-12">
                                        <textarea required name="main_description" id="main_description" class="form-control summernote-body">{{ isset($model) ? $model->main_description : old('main_description') ?? '' }}</textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="additional_description"
                                        class="col-sm-12 control-label">{{ __('posts.additional_description') }}</label>
                                    <div class="col-sm-12">
                                        <textarea name="additional_description" id="additional_description" class="form-control summernote-body">{{ isset($model) ? $model->additional_description : old('additional_description') ?? '' }}</textarea>
                                    </div>
                                </div>


                                {{-- @component('Admin.Posts.components.single-image-type', ['model' => isset($model) ? $model : null, 'filedName' => 'image_url'])
                                @endcomponent --}}

                                @component('Admin.components.single-image-picker',
                                    ['model' => isset($model) ? $model : null, 'filedName' => 'single_attachment'])
                                @endcomponent

                                @component('Admin.components.multiple-images',
                                    ['model' => isset($model) ? $model : null, 'filedName' => 'multi_attachments'])
                                @endcomponent


                                @component('Admin.components.form-actions', ['model' => isset($model) ? $model : null])
                                @endcomponent


                            </div>
                            <div class="col-md-4 col-sm-12">
                                @component('Admin.components.seo-content', ['model' => isset($model) ? $model : null])
                                @endcomponent




                                @component('Admin.Posts.components.additional-items', ['model' => isset($model) ? $model : null])
                                @endcomponent

                                <div class="form-group">
                                    <label for="slug" class="col-sm-12 control-label">{{ __('posts.slug') }} </label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="slug" id="slug" class="form-control"
                                            value="{{ isset($model) ? $model->slug : old('slug') ?? '' }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="date" class="col-sm-12 control-label">{{ __('posts.date') }}</label>
                                    <div class="col-sm-12">
                                        <input required type="date" name="date" id="date" class="form-control"
                                            value="{{ isset($model) ? \Carbon\Carbon::parse($model->date)->format('Y-m-d') : old('date') ?? \Carbon\Carbon::now()->format('Y-m-d') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="status"
                                        class="col-sm-12 control-label required">{{ __('posts.status') }}</label>
                                    <div class="col-sm-12">
                                        <select required name="status" id="status" class="form-control"
                                            data-placeholder="select a status">
                                            <option value="draft" disabled selected>{{ __('posts.Please select') }}
                                            </option>
                                            @foreach ($statuses as $status => $value)
                                                <option value="{{ $status }}"
                                                    {{ isset($model) ? ($model->status == $status ? 'selected' : old('status') ?? '') : '' }}>
                                                    {{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="author_id"
                                        class="col-sm-12 control-label">{{ __('posts.author_id') }}</label>
                                    <div class="col-sm-12">
                                        <select required name="author_id" id="author_id" class="form-control"
                                            data-placeholder="{{ __('posts.Please select') }}">
                                            <option value="0" disabled selected>{{ __('posts.Please select') }}
                                            </option>
                                            @foreach ($authors as $author)
                                                <option value="{{ $author->id }}"
                                                    {{ isset($model) ? ($model->author_id == $author->id ? 'selected' : old('author_id') ?? '') : '' }}>
                                                    {{ $author->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                @component('Admin.components.category', ['model' => isset($model) ? $model : null])
                                @endcomponent

                                @component('Admin.components.tags', ['model' => isset($model) ? $model : null])
                                @endcomponent
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    </div>
@endsection
@section('scripts')
@endsection
