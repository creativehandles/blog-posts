@extends('Admin.layout')

@section('content')
    @include('Admin.partials.form-alert')
    @include('Admin.partials.breadcumbs', ['header' => __('posts.Posts')])

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content show">
                        <div class="card-body">
                            @component('Admin.Posts.components.table', ['foo' => 'bar'])
                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/css/tables/datatable/datatables.min.css') }}">
    <script src="{{ asset('vendors/js/tables/datatable/datatables.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var theGrid = null;
        var editRoute = "{{ route(BlogPosts::getEditRoute(), ['post' => 'sampleId']) }}";
        var deleteRoute = "{{ route(BlogPosts::getDestroyRoute(), ['post' => 'sampleId']) }}";
        var gridRoute = "{{ route(BlogPosts::getGridRoute()) }}";
        $(document).ready(function() {
            theGrid = $('#thegrid').DataTable({
                "bStateSave": true,
                "language": {
                    "url": "{{ asset(__('general.dataTable')) }}"
                },
                "processing": true,
                "serverSide": true,
                "paging": true,
                "pageLength": 10,
                "ordering": true,
                "responsive": false,
                "ajax": gridRoute,
                "columnDefs": [{
                        "render": function(data, type, row) {
                            return '<a >' + data + '</a>';
                        },
                        "className": "action-col",
                        "orderable": false,
                        "targets": 0,
                        "visible": false,

                    },

                    // {
                    //     "render": function ( data, type, row ) {
                    //         return '<button href="'+editRoute.replace('sampleId',row[0])+'" class="btn btn-info btn-sm">{{ __('Update') }}</button>';
                    //     },"className":"action-col","orderable": false,
                    //     "targets": 6
                    // },
                    // {
                    //     "render": function ( data, type, row ) {
                    //         return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger btn-sm">{{ __('Delete') }}</a>';
                    //     },"className":"action-col","orderable": false,
                    //     "targets": 7
                    // },
                ]
            });
        });

        $(document).on('click', '.deleteTranslation', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var lang = $(this).data('lang');
            @if (\Illuminate\Support\Facades\Auth::user()->isSuperAdmin())
                doDelete(id, lang);
            @endif
        })


        function doDelete(id, lang) {
            // if(confirm('You really want to delete this record?')) {
            var lang = lang ? lang : '';
            swal({
                title: "{{ __('general.Warning!') }}",
                text: "{{ __('general.Are you sure you need to delete this item? this change cannot be undone.') }}",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{{ __('general.Cancel') }}",
                        value: null,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    },
                    confirm: {
                        text: "{{ __('general.Yes.Delete') }}",
                        value: !0,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    }
                }
            }).then(e => {
                if (e) {
                    $.ajax({
                        dataType: 'json',
                        method: 'delete',
                        url: deleteRoute.replace('sampleId', id) + '?delete_lang=' + lang,
                    }).done(function(response) {
                        swal("{{ __('general.Success!') }}", "{{ __('general.Item deleted!!') }}",
                            "success").then(() => {
                            theGrid.ajax.reload();
                        });
                    }).fail(function(erroErrorr) {
                        swal("{{ __('general.Error') }}", "{{ __('general.Error Occured') }}", "error");
                    });
                } else {
                    swal("{{ __('general.Cancelled') }}", "{{ __("general.It's safe") }}", "error");
                }
            });

            return false;
        }
    </script>
    <script src="{{ asset('vendors/js/extensions/sweetalert.min.js') }}" type="text/javascript"></script>

    @yield('component-scripts')
@endsection
