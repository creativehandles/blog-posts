<?php

namespace App\Http\Controllers\PackageControllers;

use Creativehandles\BlogPosts\Http\Controllers\ApiControllers\PostApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @group Blog Posts
 *
 * APIs for Blog Post
 */
class PostApiResourceController extends PostApiController
{
    public function getSearchableColumns()
    {
        return [
            "title",
            "short_description",
            "main_description",
            "additional_description"
        ]; // ['products_title', 'products_short_description']
    }

    public function getOrderableColumns()
    {
        return ["ch_posts.id" => "id", "ch_posts.title" => "title"]; // ['products.id' => 'id', "products.products_title" => "products_title"];
    }

    public function getDefaultRelations()
    {
        return ["categories", "tags"];
    }

    public function getTranslatableColumns()
    {
        return array_merge($this->getRepository()->getModel()->translatable, []); // ["categories:category_name", "categories:category_slug", "brand:title"]
    }

    /**
     * Get all Blog posts
     *
     * @queryParam search string Query to search. you can search through title or description. Example: Lorem
     * @queryParam order[0][column] string  Order column  Possible values("id","title","date","created_at","updated_at")
     * @queryParam order[0][dir] string  Possible values(asc,desc)
     *
     *
     * @queryParam where[0][column]
     * If u are using multiple where query params, it will support to AND operator in mysql
     * Possible values (id,title,short_description,status,author_id,date)
     *
     *
     * Possible where types
     *  ["table.column","relationship:column_checking"].
     *
     *
     * NOTE: above items with ":" are for relationships. relationship:column_checking
     * ex categories:title => you are looking for category name in categories relationship
     * Possible Values (categories:title,tags:name).
     *
     *
     * @queryParam with[0]  Load relationships . Example: categories
     * @queryParam with[1]  Load relationships. Example: tags
     * @queryParam with[2]  Load relationships
     * possible values (tags,categories,images). Example: images
     *
     * @queryParam where[0][operator] Possible operators (=,>,<,>=,<=,like) if u are using like make sure to add % properly in the value as it is mysql operators. Example: like
     *
     * @queryParam where[0][value] Value you are looking for. Example: %Lorem%
     *
     *
     * @queryParam with[0]  Load relationships.
     * @queryParam with[1]  Load relationships.
     *
     * possible values (tags,categories,images)
     *
     * @queryParam withoutRelations boolean drop all relations.
     *
     * Possible values (true)
     *
     * @queryParam length integer Number of items to show
     *
     * Ex link /api/v1/plugins/posts?order[0][column]=id&order[0][dir]=desc&with[0]=tags&where[0][column]=author_id&where[0][operator]==&where[0][value]=2
     *
     *
     * @responseFile storage/app/api/responses/posts.index.200.json
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        return parent::index($request);
    }

    /**
     * Get single blog post by id
     *
     * @urlParam id integer required The ID of the item
     *
     * @queryParam with[0]  Load relationships. Example: categories
     * @queryParam with[1]  Load relationships. Example: tags
     * @queryParam with[2]  Load relationships
     * possible values (tags,categories,images). Example: images
     *
     * @responseFile storage/app/api/responses/posts.show.200.json
     */
    public function show(Request $request, $id)
    {
        return parent::show($request, $id);
    }
}
