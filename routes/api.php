<?php

use Illuminate\Support\Facades\Route;

Route::group(['name' => 'blog-posts-api', 'groupName' => 'blog-posts-api','prefix'=>'plugins'], function () {
    Route::resource('posts', 'PackageControllers\PostApiResourceController')->only(['index','show']);
});

