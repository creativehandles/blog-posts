<?php

/** posts BreadCrumbs */
// Dashboard > posts
Breadcrumbs::for('admin.posts.index', function ($trail) {
    $trail->parent('dashboard');
    $trail->push(__('posts.Posts'), route('admin.posts.index'));
});

// Dashboard > posts > create
Breadcrumbs::for('admin.posts.create', function ($trail) {
    $trail->parent('admin.posts.index');
    $trail->push(__('posts.Create new'), route('admin.posts.create'));
});

// Dashboard > posts > edit
Breadcrumbs::for('admin.posts.edit', function ($trail, $post) {
    $trail->parent('admin.posts.index');
    $trail->push(__('posts.Update'), route('admin.posts.edit', $post->post ?? ''));
});
