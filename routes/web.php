<?php

use Illuminate\Support\Facades\Route;


Route::group(['name' => 'Blog Posts', 'groupName' => 'Blog Posts'], function () {
    //labels related routes
    Route::get('posts/grid', 'PackageControllers\PostCrudController@grid')->name('posts.grid');
    Route::resource('posts', 'PackageControllers\PostCrudController');
});




// Route::get('/posts/{post}', )->name('posts.show');
// Route::post('/posts', )->name('posts.store');
