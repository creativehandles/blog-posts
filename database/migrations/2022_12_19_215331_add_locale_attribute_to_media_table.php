<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;

class AddLocaleAttributeToMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            if (!Schema::hasColumn('media', 'locale')) {
                $table->string('locale')->nullable()->default(NULL);
            }
            
        });
        Role::updateOrCreate(['name' => 'Author']);
        $enMenu = \DB::table('admin_menus')->where('name','Sidebar-en')->first();
        
        if(!$enMenu){
            \DB::table('admin_menus')->insert(
                        ['name'=>'Sidebar-en']
            );
        }
        $enMenu = \DB::table('admin_menus')->where('name','Sidebar-en')->first();
        if($enMenu){
            \DB::table('admin_menu_items')->insert([
                'label'=>"Blog Posts",
                "link"=>"admin.posts.index",
                "menu"=> $enMenu->id
            ]);
        }

        $csMenu = \DB::table('admin_menus')->where('name','Sidebar-cs')->first();
        
        if(!$csMenu){
            \DB::table('admin_menus')->insert(
                        ['name'=>'Sidebar-cs']
            );
        }
        $csMenu = \DB::table('admin_menus')->where('name','Sidebar-cs')->first();
        if($csMenu){
            \DB::table('admin_menu_items')->insert([
                'label'=>"Příspěvky na blogu",
                "link"=>"admin.posts.index",
                "menu"=> $csMenu->id
            ]);
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->dropColumn('locale');
        });
    }
}