<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ch_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longText('short_description')->nullable()->default(NULL);
            $table->longText('main_description')->nullable()->default(NULL);
            $table->longText('additional_description')->nullable()->default(NULL);
            $table->longText('image_url')->nullable()->default(NULL);

            $table->string('slug');
            $table->date('date')->nullable()->default(now());
            $table->string('status')->nullable()->default("draft");

            $table->integer('author_id')->index()->nullable()->default(NULL);
            $table->string('reading_time')->nullable()->default(NULL);

            $table->string('seo_title')->nullable()->default(NULL);
            $table->longText('seo_description')->nullable()->default(NULL);
            $table->text('seo_url')->nullable()->default(NULL);
            $table->text('seo_image_url')->nullable()->default(NULL);
            $table->text('seo_keywords')->nullable()->default(NULL);

            $table->timestamps();
            $table->string('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_posts');
    }
}
