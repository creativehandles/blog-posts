<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterChPostsTableForTranslationSupport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ch_posts', function (Blueprint $table) {
            $table->json('title')->change();
            $table->json('short_description')->change();
            $table->json('main_description')->change();
            $table->json('additional_description')->change();

            $table->json('slug')->change();
            $table->json('status')->change();

            $table->json('reading_time')->change();

            $table->json('seo_title')->change();
            $table->json('seo_description')->change();
            $table->json('seo_url')->change();
            $table->json('seo_image_url')->change();
            $table->json('seo_keywords')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ch_posts');
    }
}
